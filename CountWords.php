<?php

declare(strict_types=1);

class CountWords
{
    public function __construct(
        private string $str,
        private int $minSymbols = 3,
        private int $maxResult = 10
    ) {
    }

    public function getCount(): array
    {
        $wordsArray = $this->splitStringIntoWords();
        $sorterWordsArray = $this->alphabeticalSorting($wordsArray);
        return $this->firstSplice($sorterWordsArray);
    }

    private function splitStringIntoWords(): array
    {
        foreach ($this->prepareString($this->str) as $item) {
            if ($this->checkMinSymbols($item) === true) {
                continue;
            }

            $array[$item] = !empty($array[$item]) ? ($array[$item] + 1) : 1;
        }

        return $array ?? [];
    }

    private function alphabeticalSorting(array $array): array
    {
        uasort($array, function($a, $b) {
            if ($a === $b) {
                return 0;
            }

            return ($a < $b) ? 1 : -1;
        });

        return $array;
    }

    private function firstSplice(array $array): array
    {
        $arrayNew = [];

        foreach ($array as $k => $item) {
            if (count($arrayNew) === $this->maxResult) {
                break;
            }

            $arrayNew[$k] = $item;
        }

        return $arrayNew;
    }

    private function prepareString(string $str): array
    {
        $str = mb_strtolower($str);
        $str = preg_replace('/[^\w\s]/ui', '', $str);
        return explode(' ', $str);
    }

    private function checkMinSymbols(string $item): bool
    {
        return mb_strlen($item) <= $this->minSymbols;
    }
}
